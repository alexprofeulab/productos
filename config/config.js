
const config = {
    db: {
        host: "localhost",
        user: "tienda_user",
        password: "tienda",
        database: "tienda"
    }, 
    directories: {
        images: 'http://localhost/imgs'
    }
};

module.exports = config;