const mime = require('mime');
var fs = require('fs');

function decodeBase64Image(dataString) {
    let matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
    response = {};
    
    if (matches.length !== 3) {
        return new Error('Invalid input string');
    }
    
    response.type = matches[1];
    response.data = new Buffer(matches[2], 'base64');
    
    return response;
}

module.exports.guardaImagenBase64 = function (imagenBase64, fileNameWithoutExtension) {
    var decodedImg = decodeBase64Image(imagenBase64);
    var imageBuffer = decodedImg.data;
    var type = decodedImg.type;
    var extension = mime.getExtension(type);
    var fileName =  fileNameWithoutExtension + "." + extension;

    try{
        fs.writeFileSync(__dirname + "/../public/imgs/" + fileName, imageBuffer, 'utf8');
        return fileName; 
    }
    catch(err){
        return false;
    }
}