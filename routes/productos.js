const express = require('express');
const conexion = require(__dirname + '/../database/conexion');
const utilImages = require(__dirname + '/../utils/util-images');
const config = require(__dirname + '/../config/config');

let router = express.Router();

router.get('/', (req, res) => {
    conexion.query("SELECT * FROM productos",
        (error, resultado, campos) => {
            if (error) {
                res.status(500).send({
                    ok: false,
                    mensaje: "Error listando productos"
                });
            }
            else {
                res.status(200).send({
                    ok: true,
                    mensaje: "Se han obtenido los productos correctamente",
                    data: resultado
                });
            }
        });
});

router.get('/:id', (req, res) => {
    conexion.query("SELECT * FROM productos WHERE id=?", req.params.id,
        (error, resultado, campos) => {
            if (error) {
                res.status(500).send({
                    ok: false,
                    mensaje: "Error obteniendo el producto"
                });
            }
            else {
                if (resultado.length === 0) {
                    res.status(404).send({
                        ok: false,
                        mensaje: "No existe ningún producto con id " + req.params.id,
                    });
                }
                else {
                    res.status(200).send({
                        ok: true,
                        mensaje: "Se ha obtenido el producto " + req.params.id + " correctamente",
                        data: resultado[0]
                    });
                }
            }
        });
});

router.delete('/:id', (req, res) => {
    conexion.query("DELETE FROM productos WHERE id = ?",
        req.params.id, (error, resultado, campos) => {
            if (error) {
                res.status(500)
                    .send({
                        ok: false,
                        error: "Error eliminando el producto " + req.params.id
                    });
            }
            else {
                if (resultado.affectedRows === 0) {
                    res.status(404).send({
                        ok: false,
                        mensaje: "El producto " + req.params.id + " no existe"
                    });
                }
                else {
                    res.status(200).send({
                        ok: true,
                        mensaje: "El producto " + req.params.id + " Ha sido eliminado correctamente"
                    });
                }
            }
        });
});

router.post('/', (req, res) => {
    let nuevoproducto = {
        id: req.body.id,
        available: req.body.available,
        description: req.body.description,
        imageUrl: req.body.imageUrl,
        price: req.body.price,
        rating: req.body.rating
    };

    fileName = utilImages.guardaImagenBase64(nuevoproducto.imageUrl, nuevoproducto.id);
    if (fileName === false) {
        res.status(500).send({
            ok: false,
            mensaje: "Error guardando el fichero de imagen"
        });
    }
    else {
        nuevoproducto.imageUrl = config.directories.images + '/' + fileName;

        conexion.query("INSERT INTO productos SET ?",
            nuevoproducto, (error, resultado, campos) => {
                if (error)
                    res.status(400)
                        .send({
                            ok: false,
                            mensaje: "Error insertando el nuevo producto"
                        });
                else {
                    conexion.query("SELECT * FROM productos WHERE id=?", resultado.insertId,
                        (error, resultado, campos) => {
                            if (error) {
                                res.status(500).send({
                                    ok: false,
                                    mensaje: "Error obteniendo el producto"
                                });
                            }
                            else {
                                if (resultado.length === 0) {
                                    res.status(404).send({
                                        ok: false,
                                        mensaje: "No existe ningún producto con id " + req.params.id,
                                    });
                                }
                                else {
                                    res.status(200).send({
                                        ok: true,
                                        mensaje: "Se ha obtenido el producto " + req.params.id + " correctamente",
                                        data: resultado[0]
                                    });
                                }
                            }
                        });
                }
            });
    }
});

router.put('/:id', (req, res) => {
    let productoEditado = {
        id: req.body.id,
        available: req.body.available,
        description: req.body.description,
        imageUrl: req.body.imageUrl,
        price: req.body.price,
        rating: req.body.rating
    };

    if (productoEditado.ImageUrl.length > 0) {
        fileName = utilImages.guardaImagenBase64(productoEditado.ImageUrl, productoEditado.id);
        if (fileName === false) {
            res.status(500).send({
                ok: false,
                mensaje: "Error guardando el fichero de imagen"
            });
        }
    
        productoEditado.ImageUrl = config.directories.images + '/' + fileName;
    }

    conexion.query("UPDATE productos SET ? WHERE COD = ?",
        [ productoEditado, productoEditado.COD ], (error, resultado, campos) => {
            if (error)
                res.status(400)
                    .send({
                        ok: false,
                        mensaje: "Error editando el nuevo producto. " + error
                    });
            else
                res.status(200)
                    .send({
                        ok: true,
                        mensaje: resultado
                    });
        });
});

router.put('/rating/:id', (req, res) => {
    let obj = {
        rating: req.body.rating
    };

    conexion.query("UPDATE productos SET ? WHERE id = ?",
        [ obj, req.params.id ], (error, resultado, campos) => {
            if (error)
                res.status(400)
                    .send({
                        ok: false,
                        mensaje: "Error editando el nuevo producto. " + error
                    });
            else
                res.status(200)
                    .send({
                        ok: true,
                        mensaje: resultado
                    });
        });
});

module.exports = router;