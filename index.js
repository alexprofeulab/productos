const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const productos = require(__dirname + '/routes/productos');

let app = express();

app.use(cors());
app.use(bodyParser.json({limit: '50mb'}));
app.use('/productos', productos);

app.use('/', express.static(__dirname + '/public'));

app.listen(80);